import Cookies from 'js-cookie'

class LocaleConfig {
  /**
   * Dictionary for lookup.
   */
  dict: any;

  /**
   * Cookie name for preferred locale.
   * @default Default value is `locale`.
   */
  cookieKey: string;
}

let _config: LocaleConfig = {
  dict: {},
  cookieKey: 'locale'
};

/**
 * Sets global locale config.
 */
export function setLocaleConfig(config: Partial<LocaleConfig>) {
  _config = {
    ..._config,
    ...config
  };

  if (config.dict) {
    initDictionary(config.dict);
  }
}

let state = {
  lowerCaseDict: {} as any,
};

function initDictionary(dict: any) {
  for (let key of Object.keys(dict)) {
    state.lowerCaseDict[key.toLowerCase()] = dict[key];
  }
}

/**
 * @returns Whether the given string is entriely upper-case.
 */
function isUpperCase(str: string) {
  return str.toUpperCase() === str;
}

const transformFirstCharacter = false;

/**
 * @returns a localized string from a dictionary look-up.
 */
export function lookup(phraseOrKey: string | TemplateStringsArray, ...args: any[]): string {
  const variablePlaceholder = '__'
  const hasArgs = args && args.length

  const { dict } = _config;
  const { lowerCaseDict } = state;

  if (!lowerCaseDict) {
    throw new Error(`You need to invoke setLocaleConfig before any import which might utilize lookup.`);
  }

  if (Array.isArray(phraseOrKey)) {
    //  is a template string
    phraseOrKey = (phraseOrKey as TemplateStringsArray).join(variablePlaceholder);
  } else {
    phraseOrKey = phraseOrKey.toString()
  }
  var foundMatch = true
  var match = dict[phraseOrKey];

  if (!match) {
    //  lower-case lookup
    var lcPhrase = phraseOrKey.toLowerCase()
    match = lowerCaseDict[lcPhrase]

    if (!match) {
      //  without periods
      match = lowerCaseDict[lcPhrase.replace(/\./ig, '')];

      if (!match) {
        foundMatch = false
      }
    }
  }
  var locale = getLocaleSetting()
  var result = foundMatch ? match[locale] || match["en"] || phraseOrKey : phraseOrKey;

  if (foundMatch && transformFirstCharacter) {
    if (match[locale]) {
      var isPhrase = !/[a-z][A-Z]/.test(phraseOrKey)
      if (isPhrase) {
        //  transform first match character
        result = (
          result.substr(0, 1)
        )[isUpperCase(phraseOrKey.charAt(0)) ? "toUpperCase" : "toLowerCase"]() + result.substr(1);
      }
    }
  }

  //  support for elements
  var resultWithArgs = []

  if (hasArgs) {
    var resultSplit = result.split(variablePlaceholder);
    resultWithArgs.push(resultSplit[0])

    //  replace variable placeholders
    for (var i = 0, n = args.length; i < n; i++) {
      resultWithArgs.push(args[i])
      resultWithArgs.push(resultSplit[i + 1])
    }

    //  support for elements
    var argsAreString = true
    for (let arg of args) {
      if (typeof arg !== "string") {
        argsAreString = false
        break
      }
    }

    if (argsAreString) {
      result = resultWithArgs.join("")
    } else {
      //  keep array to enable support for jsx elements
      result = resultWithArgs
    }
  }

  return result
}

export type SupportedLocale = 'en' | 'sv';
const localeKey = () => _config.cookieKey;

//  used to improve performance
var localSettingCache: SupportedLocale
const defaultLocalSetting: SupportedLocale = 'sv'

/**
 * @returns The current locale settings.
 */
export function getLocaleSetting(): SupportedLocale {
  if (localSettingCache == null) {
    localSettingCache = Cookies.get(localeKey()) as SupportedLocale || defaultLocalSetting
  }
  return localSettingCache
}

/**
 * @returns A four-letter code representation of a locale.
 */
export function getLocaleCode(locale = getLocaleSetting()): String {
  return {
    "sv": "sv-SE",
    "en": "en-US"
  }[locale] || null;
}

/**
 * Sets the current locale.
 */
export function setLocaleSetting(locale: SupportedLocale) {
  if (getLocaleSetting() !== locale) {
    localSettingCache = locale;
    Cookies.set(localeKey(), locale)
    dispatchEvent('change', locale)
  }
}

type Listener = (...args: any[]) => void
type LocalizationEvent = 'change';
var listeners: {
  [event: string]: Set<Listener>
} = {};

function dispatchEvent(ev: LocalizationEvent, ...args: any[]) {
  const a = listeners[ev]
  if (a instanceof Set) {
    a.forEach(e => e(...args));
  }
}

export const Localization = {
  on: (event: LocalizationEvent, listener: Listener) => {
    listeners[event as any] = listeners[event] || new Set<Listener>();
    listeners[event].add(listener)

    return () => {
      listeners[event].delete(listener)
    }
  },
}


/**
 * Attempts to convert an error into a readable error message.
 * @param e Response describing the error
 * @param context A context to help identify the error
 */
export function getLocalizedErrorMessage(e?: { error?: string, message?: string, extra?: string }, context?: any, opts = {
  genericMessage: "Ett tillfälligt fel har uppstått"
}): string {
  let error = e.error || e.message
  if (!e || (!error && !e.extra)) {
    return null
  }

  if (typeof context === "string") {
    context = context.toLowerCase()
  }

  const { extra, field } = e as any

  switch (error) {
    case 'missingParams':
    case 'validationError':
      if (/`date`/i.test(extra)) {
        return lookup`Enter a valid date.`
      }
      break;
  }

  return opts.genericMessage
}
