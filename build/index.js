"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var js_cookie_1 = __importDefault(require("js-cookie"));
var LocaleConfig = /** @class */ (function () {
    function LocaleConfig() {
    }
    return LocaleConfig;
}());
var _config = {
    dict: {},
    cookieKey: 'locale'
};
/**
 * Sets global locale config.
 */
function setLocaleConfig(config) {
    _config = __assign(__assign({}, _config), config);
    if (config.dict) {
        initDictionary(config.dict);
    }
}
exports.setLocaleConfig = setLocaleConfig;
var state = {
    lowerCaseDict: {},
};
function initDictionary(dict) {
    for (var _i = 0, _a = Object.keys(dict); _i < _a.length; _i++) {
        var key = _a[_i];
        state.lowerCaseDict[key.toLowerCase()] = dict[key];
    }
}
/**
 * @returns Whether the given string is entriely upper-case.
 */
function isUpperCase(str) {
    return str.toUpperCase() === str;
}
var transformFirstCharacter = false;
/**
 * @returns a localized string from a dictionary look-up.
 */
function lookup(phraseOrKey) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    var variablePlaceholder = '__';
    var hasArgs = args && args.length;
    var dict = _config.dict;
    var lowerCaseDict = state.lowerCaseDict;
    if (!lowerCaseDict) {
        throw new Error("You need to invoke setLocaleConfig before any import which might utilize lookup.");
    }
    if (Array.isArray(phraseOrKey)) {
        //  is a template string
        phraseOrKey = phraseOrKey.join(variablePlaceholder);
    }
    else {
        phraseOrKey = phraseOrKey.toString();
    }
    var foundMatch = true;
    var match = dict[phraseOrKey];
    if (!match) {
        //  lower-case lookup
        var lcPhrase = phraseOrKey.toLowerCase();
        match = lowerCaseDict[lcPhrase];
        if (!match) {
            //  without periods
            match = lowerCaseDict[lcPhrase.replace(/\./ig, '')];
            if (!match) {
                foundMatch = false;
            }
        }
    }
    var locale = getLocaleSetting();
    var result = foundMatch ? match[locale] || match["en"] || phraseOrKey : phraseOrKey;
    if (foundMatch && transformFirstCharacter) {
        if (match[locale]) {
            var isPhrase = !/[a-z][A-Z]/.test(phraseOrKey);
            if (isPhrase) {
                //  transform first match character
                result = (result.substr(0, 1))[isUpperCase(phraseOrKey.charAt(0)) ? "toUpperCase" : "toLowerCase"]() + result.substr(1);
            }
        }
    }
    //  support for elements
    var resultWithArgs = [];
    if (hasArgs) {
        var resultSplit = result.split(variablePlaceholder);
        resultWithArgs.push(resultSplit[0]);
        //  replace variable placeholders
        for (var i = 0, n = args.length; i < n; i++) {
            resultWithArgs.push(args[i]);
            resultWithArgs.push(resultSplit[i + 1]);
        }
        //  support for elements
        var argsAreString = true;
        for (var _a = 0, args_1 = args; _a < args_1.length; _a++) {
            var arg = args_1[_a];
            if (typeof arg !== "string") {
                argsAreString = false;
                break;
            }
        }
        if (argsAreString) {
            result = resultWithArgs.join("");
        }
        else {
            //  keep array to enable support for jsx elements
            result = resultWithArgs;
        }
    }
    return result;
}
exports.lookup = lookup;
var localeKey = function () { return _config.cookieKey; };
//  used to improve performance
var localSettingCache;
var defaultLocalSetting = 'sv';
/**
 * @returns The current locale settings.
 */
function getLocaleSetting() {
    if (localSettingCache == null) {
        localSettingCache = js_cookie_1.default.get(localeKey()) || defaultLocalSetting;
    }
    return localSettingCache;
}
exports.getLocaleSetting = getLocaleSetting;
/**
 * @returns A four-letter code representation of a locale.
 */
function getLocaleCode(locale) {
    if (locale === void 0) { locale = getLocaleSetting(); }
    return {
        "sv": "sv-SE",
        "en": "en-US"
    }[locale] || null;
}
exports.getLocaleCode = getLocaleCode;
/**
 * Sets the current locale.
 */
function setLocaleSetting(locale) {
    if (getLocaleSetting() !== locale) {
        localSettingCache = locale;
        js_cookie_1.default.set(localeKey(), locale);
        dispatchEvent('change', locale);
    }
}
exports.setLocaleSetting = setLocaleSetting;
var listeners = {};
function dispatchEvent(ev) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    var a = listeners[ev];
    if (a instanceof Set) {
        a.forEach(function (e) { return e.apply(void 0, args); });
    }
}
exports.Localization = {
    on: function (event, listener) {
        listeners[event] = listeners[event] || new Set();
        listeners[event].add(listener);
        return function () {
            listeners[event].delete(listener);
        };
    },
};
/**
 * Attempts to convert an error into a readable error message.
 * @param e Response describing the error
 * @param context A context to help identify the error
 */
function getLocalizedErrorMessage(e, context, opts) {
    if (opts === void 0) { opts = {
        genericMessage: "Ett tillfälligt fel har uppstått"
    }; }
    var error = e.error || e.message;
    if (!e || (!error && !e.extra)) {
        return null;
    }
    if (typeof context === "string") {
        context = context.toLowerCase();
    }
    var _a = e, extra = _a.extra, field = _a.field;
    switch (error) {
        case 'missingParams':
        case 'validationError':
            if (/`date`/i.test(extra)) {
                return lookup(templateObject_1 || (templateObject_1 = __makeTemplateObject(["Enter a valid date."], ["Enter a valid date."])));
            }
            break;
    }
    return opts.genericMessage;
}
exports.getLocalizedErrorMessage = getLocalizedErrorMessage;
var templateObject_1;
