declare class LocaleConfig {
    /**
     * Dictionary for lookup.
     */
    dict: any;
    /**
     * Cookie name for preferred locale.
     * @default Default value is `locale`.
     */
    cookieKey: string;
}
/**
 * Sets global locale config.
 */
export declare function setLocaleConfig(config: Partial<LocaleConfig>): void;
/**
 * @returns a localized string from a dictionary look-up.
 */
export declare function lookup(phraseOrKey: string | TemplateStringsArray, ...args: any[]): string;
export declare type SupportedLocale = 'en' | 'sv';
/**
 * @returns The current locale settings.
 */
export declare function getLocaleSetting(): SupportedLocale;
/**
 * @returns A four-letter code representation of a locale.
 */
export declare function getLocaleCode(locale?: SupportedLocale): String;
/**
 * Sets the current locale.
 */
export declare function setLocaleSetting(locale: SupportedLocale): void;
declare type Listener = (...args: any[]) => void;
export declare const Localization: {
    on: (event: "change", listener: Listener) => () => void;
};
/**
 * Attempts to convert an error into a readable error message.
 * @param e Response describing the error
 * @param context A context to help identify the error
 */
export declare function getLocalizedErrorMessage(e?: {
    error?: string;
    message?: string;
    extra?: string;
}, context?: any, opts?: {
    genericMessage: string;
}): string;
export {};
